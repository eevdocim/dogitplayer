﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Net.Http;
using Newtonsoft.Json;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DogitPlayer
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static readonly HttpClient client = new HttpClient();
        List<Artists> artists = new List<Artists>();
        List<Albums> albums = new List<Albums>();
        List<Tracks> tracks = new List<Tracks>();
        int isPlayingArtist = -1;
        int isPlayingAlbums = -1;
        int isPlayingTrack = -1;
        public MainWindow()
        {
            InitializeComponent();
            volumeAudio.Value = 0.2;
            LoadArtists();
        }

        public async void LoadArtists()
        {
            try
            {
                rootWindow.Children.Clear();
                string param = "?function=artists";
                HttpResponseMessage response = await client.GetAsync("https://dogit-studios.ru/StreamPlayer/api"+param);
                string responseBody = await response.Content.ReadAsStringAsync();
                artists = JsonConvert.DeserializeObject<List<Artists>>(responseBody);
                int y = 0;
                foreach (Artists artist in artists)
                {
                    Console.WriteLine(artist.id);
                    Button btn = new Button();
                    btn.Content = artist.name;
                    btn.Height = 50;
                    btn.Background = Brushes.White;
                    btn.Margin = new Thickness(0, y, 0, 0);
                    y += 50;
                    btn.Click += (s, e) =>
                    {
                        isPlayingArtist = artists.IndexOf(artist);
                        LoadAlbums(artist.id);
                    };
                    btn.VerticalAlignment = VerticalAlignment.Top;
                    rootWindow.Children.Add(btn);
                }
                Console.WriteLine(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }

        public async void LoadAlbums(int id_artist)
        {
            try
            {
                rootWindow.Children.Clear();
                string param = "?function=albums&id_artist=" + id_artist;
                HttpResponseMessage response = await client.GetAsync("https://dogit-studios.ru/StreamPlayer/api"+param);
                string responseBody = await response.Content.ReadAsStringAsync();
                albums = JsonConvert.DeserializeObject<List<Albums>>(responseBody);
                int y = 0;
                foreach (Albums album in albums)
                {
                    Console.WriteLine(album.id);
                    Button btn = new Button();
                    btn.Content = album.name;
                    btn.Height = 50;
                    btn.Background = Brushes.White;
                    btn.Margin = new Thickness(0, y, 0, 0);
                    y += 50;
                    btn.Click += (s, e) =>
                    {
                        isPlayingAlbums = albums.IndexOf(album);
                        LoadTracks(album.id, id_artist);
                    };
                    btn.VerticalAlignment = VerticalAlignment.Top;
                    rootWindow.Children.Add(btn);
                }
                Button btn_back = new Button();
                btn_back.Content = "<<";
                btn_back.Height = 50;
                btn_back.Background = Brushes.White;
                btn_back.Margin = new Thickness(0, y, 0, 0);
                y += 50;
                btn_back.Click += (s, e) => LoadArtists();
                btn_back.VerticalAlignment = VerticalAlignment.Top;
                rootWindow.Children.Add(btn_back);
                Console.WriteLine(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }

        public async void LoadTracks(int id_album, int id_artist)
        {
            try
            {
                rootWindow.Children.Clear();
                string param = "?function=tracks&id_album="+id_album;
                HttpResponseMessage response = await client.GetAsync("https://dogit-studios.ru/StreamPlayer/api" + param);
                string responseBody = await response.Content.ReadAsStringAsync();
                tracks = JsonConvert.DeserializeObject<List<Tracks>>(responseBody);
                int y = 0;
                foreach (Tracks track in tracks)
                {
                    Console.WriteLine(track.id);
                    Button btn = new Button();
                    btn.Content = track.name;
                    btn.Height = 50;
                    btn.Background = Brushes.White;
                    btn.Margin = new Thickness(0, y, 0, 0);
                    y += 50;
                    btn.Click += (s, e) =>
                    {
                        isPlayingTrack = tracks.IndexOf(track);
                        PlayTrack(track.id);
                    };
                    btn.VerticalAlignment = VerticalAlignment.Top;
                    rootWindow.Children.Add(btn);
                }
                Button btn_back = new Button();
                btn_back.Content = "<<";
                btn_back.Height = 50;
                btn_back.Background = Brushes.White;
                btn_back.Margin = new Thickness(0, y, 0, 0);
                y += 50;
                btn_back.Click += (s, e) => LoadAlbums(id_artist);
                btn_back.VerticalAlignment = VerticalAlignment.Top;
                rootWindow.Children.Add(btn_back);
                Console.WriteLine(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }

        public async void PlayTrack(int id_track)
        {
            try
            {
                string param = "?function=track_play&id_track="+id_track;
                HttpResponseMessage response = await client.GetAsync("https://dogit-studios.ru/StreamPlayer/api" + param);
                string responseBody = await response.Content.ReadAsStringAsync();
                Console.WriteLine(tracks[isPlayingTrack].name);
                nameTrack.Content = tracks[isPlayingTrack].name;
                audio.Source =  new Uri(responseBody);
                audio.Volume = volumeAudio.Value;
                controlAudio.Content = "Pause";
                audio.Play();

                Console.WriteLine(responseBody);
                
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            audio.Volume = e.NewValue;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Console.WriteLine(e.Source);
            Button button = (Button)sender;
            if (isPlayingTrack != -1)
            {
                if (button.Content.ToString() == "Pause")
                {
                    button.Content = "Play";
                    audio.Pause();
                }
                else
                {
                    button.Content = "Pause";
                    audio.Play();
                }
            }
            else
            {

            }
            
        }

        private void volumeAudio_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if(e.Delta > 0)
            {
                volumeAudio.Value += 0.01;
            }
            else
            {
                volumeAudio.Value -= 0.01;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (isPlayingTrack <= 0)
            {
                isPlayingTrack = tracks.Count -1;
            }
            else
            {
                isPlayingTrack -= 1;
            }
            PlayTrack(tracks[isPlayingTrack].id);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (isPlayingTrack >= tracks.Count-1)
            {
                isPlayingTrack = 0;
            }
            else
            {
                isPlayingTrack += 1;
            }
            PlayTrack(tracks[isPlayingTrack].id);
        }

        private void audio_MediaEnded(object sender, RoutedEventArgs e)
        {
            if (isPlayingTrack >= tracks.Count - 1)
            {
                isPlayingTrack = 0;
            }
            else
            {
                isPlayingTrack += 1;
            }
            PlayTrack(tracks[isPlayingTrack].id);
        }
    }
}
